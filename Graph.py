
class Vertex :
    def __init__(self, vid, tile, quadrant):
        self.vid = vid
        self.sibling = []
        self.tile = tile
        self.quadrant = quadrant
        self.links = []

class Link :
    def __init__(self, src, weight, vertex):
        self.src = src
        self.weight = weight
        self.vertex = vertex

class Graph :
    def __init__(self):
        self.vertexes = set()
        self.by_id = {}
        self.by_quadrant = {}
        self.links = []
    
    def hash_from_quadrant(self, quadrant):
        return quadrant.x + 100 * quadrant.y
    
    def add_vertex(self, vid, tile, quadrant):
        v = Vertex(vid, tile, quadrant)
        self.vertexes.add(v)
        self.by_id[vid] = v
        self.by_quadrant[self.hash_from_quadrant(quadrant)] = v
        return v
    
    def vertex_by_quadrant(self, quadrant):
        return self.by_quadrant[self.hash_from_quadrant(quadrant)]
    
    def vertex_by_id(self,vid):
        return self.by_id[vid]
    
    def link(self, src, dst, weight):
        link = Link(src, weight, dst)
        src.links.append(link)
        self.links.append(link)