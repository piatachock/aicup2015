from model.Car import Car
from model.Game import Game
from model.Move import Move
from model.World import World
from model.TileType import TileType

from math import copysign, sin, cos, sqrt
from dijkstra import dijkstra, State
from Quadrant import Quadrant, get_quadrant
from Graph import Vertex, Link, Graph
from PID import PidRegulator
from IPython.external.path._path import path

class Point :
    def __init__(self, coords, vertex, vector = (0,0)):
        self.x = coords[0]
        self.y = coords[1]
        self.vertex = vertex
        self.dx = vector[0];
        self.dy = vector[1]

def neigbours_dispatch(width, heigh):
    def left(x, y):
        return Quadrant(x-1, y) if x > 0 else None
    def right(x, y):
        return Quadrant(x+1, y) if x < width-1 else None
    def top(x, y):
        return Quadrant(x, y-1) if y > 0 else None
    def bottom(x, y):
        return Quadrant(x, y+1) if y < heigh-1 else None

    return { TileType.EMPTY : lambda x, y : [],
        TileType.UNKNOWN : lambda x, y : [],
        TileType.VERTICAL : lambda x, y : [top(x, y), bottom(x, y)],
        TileType.HORIZONTAL : lambda x, y : [left(x, y), right(x, y)],
        TileType.LEFT_TOP_CORNER : lambda x, y : [bottom(x, y), right(x, y)],
        TileType.RIGHT_TOP_CORNER : lambda x, y : [bottom(x, y), left(x, y)],
        TileType.LEFT_BOTTOM_CORNER : lambda x, y : [top(x, y), right(x, y)],
        TileType.RIGHT_BOTTOM_CORNER : lambda x, y : [top(x, y), left(x, y)],
        TileType.LEFT_HEADED_T : lambda x, y : [top(x, y), left(x, y), bottom(x, y)],
        TileType.RIGHT_HEADED_T : lambda x, y : [top(x, y), right(x, y), bottom(x, y)],
        TileType.TOP_HEADED_T : lambda x, y : [top(x, y), right(x, y), left(x, y)],
        TileType.BOTTOM_HEADED_T : lambda x, y : [bottom(x, y), right(x, y), left(x, y)],
        TileType.CROSSROADS : lambda x, y : [top(x, y), bottom(x, y), right(x, y), left(x, y)],
    }

TILE_2_STR = { TileType.EMPTY : "EMPTY",
        TileType.UNKNOWN : "UNKNOWN",
        TileType.VERTICAL : "VERTICAL",
        TileType.HORIZONTAL : "HORIZONTAL",
        TileType.LEFT_TOP_CORNER : "LEFT_TOP_CORNER",
        TileType.RIGHT_TOP_CORNER : "RIGHT_TOP_CORNER",
        TileType.LEFT_BOTTOM_CORNER : "LEFT_BOTTOM_CORNER",
        TileType.RIGHT_BOTTOM_CORNER : "RIGHT_BOTTOM_CORNER",
        TileType.LEFT_HEADED_T : "LEFT_HEADED_T",
        TileType.RIGHT_HEADED_T : "RIGHT_HEADED_T",
        TileType.TOP_HEADED_T : "TOP_HEADED_T",
        TileType.BOTTOM_HEADED_T : "BOTTOM_HEADED_T",
        TileType.CROSSROADS : "CROSSROADS",
    }

def make_simple_graph(tiles, width, heigh):
    def weight(src, dst):
        return 1

    graph = Graph()
    get_neigbours = neigbours_dispatch(width, heigh)
    links = []
    for x in range(0, width):
        for y in range(0, heigh):
            tile = tiles[x][y]
            graph.add_vertex(x+y*100, tile, Quadrant(x, y))
            n = [i for i in get_neigbours[tile](x, y) if i is not None]
            if len(n) != 0:
                links.append((Quadrant(x,y),n))

    for q, neigbours in links :
        src = graph.vertex_by_quadrant(q)
        for n in neigbours :
            if n is not None :
                dst = graph.vertex_by_quadrant(n)
                graph.link(src, dst, weight(src, dst))
        print (src.vid, [i.vertex.vid for i in src.links ])
    
    return graph

def strip_path(path):
    if len(path) > 2 :
        tripes = []
        for i in range(0, len(path)-2):
            s=path[i:i+3]
            dx_1 = s[1].x - s[0].x
            dy_1 = s[1].y - s[0].y
            dx = s[2].x - s[1].x
            dy = s[2].y - s[1].y
            if dx_1 != dx and dy_1 != dy :
                tripes.append(s[1:2])
        retval = []
        for s in tripes:
            for i in s:
                if len(retval) == 0 or retval[-1] is not i:
                    retval.append(i)
        if retval[-1] is not path[-1]:
            retval.append(path[-1])
        return retval
    return path

def get_points(path):
    def point(l):
        return Point(l.vertex.quadrant.middle(), l.vertex)
    def mark_point(p, dx, dy):
        p.x, p.y = p.vertex.quadrant.at(dx, dy)
    if len(path) > 2 :
        retval = [point(path[0])]
        for i in range(0, len(path)-2):
            s=[point(l) for l in path[i:i+3]]
            dx_1 = s[1].x - s[0].x
            dy_1 = s[1].y - s[0].y
            dx = s[2].x - s[1].x
            dy = s[2].y - s[1].y
            '''
            if dx_1 > dx or dy_1 < dy : #Turn left
                pass 
            if dx_1 > dx or dy_1 > dy : #Turn right
                pass
            '''
            if (dx_1 < 0 and dy > 0) or (dy_1 < 0 and dx > 0) :
                #LEFT_TOP_CORNER
                mark_point( s[1], 0.75, 0.75)
            elif (dx_1 > 0 and dy > 0) or (dy_1 < 0 and dx < 0) :
                #RIGHT_TOP_CORNER
                mark_point(s[1], 0.25, 0.75)
            elif (dx_1 < 0 and dy < 0) or (dy_1 > 0 and dx > 0) :
                #LEFT_BOTTOM_CORNER
                mark_point(s[1], 0.75, 0.25)
            elif (dx_1 > 0 and dy < 0) or (dy_1 > 0 and dx < 0) :
                #RIGHT_BOTTOM_CORNER
                mark_point(s[1], 0.25, 0.25)
            elif dx_1 > 0 and dx > 0 :
                mark_point(s[1], 0.5, 0.5)
            elif dx_1 < 0 and dx < 0 :
                mark_point(s[1], 0.5, 0.5)
            elif dy_1 > 0 and dy > 0 :
                mark_point(s[1], 0.5, 0.5)
            elif dy_1 < 0 and dy < 0 :
                mark_point(s[1], 0.5, 0.5)
            retval.append(s[1])
        retval.append(point(path[-1]))
        return retval
    return [point(l) for l in path]

def get_feasible_path(path):
    return strip_path(get_points(path))

def get_nearest_point(path):
    return path[0]

class MyStrategy:
    def __init__(self):
        self.graph = None
        self.angular_regulator = PidRegulator(2.0, 0.0, 0.0, 0.150)
        self.linear_regulator = PidRegulator(0.03, 0.0001, 0.005, 0.150)
        self.f = open('error.txt', 'w')
        self.target = None
    
    def way_point_node(self, world, index):
        if index >= len(world.waypoints) :
            return None
        p = world.waypoints[index]
        return 

    def move(self, me, world, game, move):
        """
        @type me: Car
        @type world: World
        @type game: Game
        @type move: Move
        """
        wp_index = me.next_waypoint_index
        waypoints = world.waypoints + world.waypoints
        Quadrant.size = game.track_tile_size

        if self.graph is None :
            self.graph = make_simple_graph( world.tiles_x_y, world.width, world.height )

        speed = sqrt(me.speed_x ** 2 + me.speed_y ** 2)
        my_node = self.graph.vertex_by_quadrant(get_quadrant(me))
        current_node = my_node
        path = []
        last_state = State(me.angle, speed);
        for wp in waypoints[wp_index:] :
            next_node = self.graph.vertex_by_quadrant(Quadrant(wp[0], wp[1]))
            c, s, p = dijkstra(self.graph, current_node, last_state, next_node)
            path = path[:-1] + p
            current_node = next_node
            last_state = s

        path = get_feasible_path(path)
        p = get_nearest_point(path)
        if self.target is None or self.target.vertex.vid != p.vertex.vid :
            print "%i (%i,%i) %s)" % (p.vertex.vid, p.x, p.y, TILE_2_STR[p.vertex.tile])
            print "->".join(["%i" % i.vertex.vid for i in path ])
            self.target = p

        angle = me.get_angle_to(p.x, p.y)

        wu = self.angular_regulator.control(angle)
        move.wheel_turn = wu
        self.f.write("%i\t%f\t%f\n" % (world.tick,angle,move.wheel_turn))

        distance = me.get_distance_to(p.x, p.y)

        uv = self.linear_regulator.control(distance)
        ev = uv - speed
        ua = 0.5 * (uv * cos(angle * 1.3) - speed)

        ep = ua - me.engine_power
        move.engine_power = ua
        #print (ev, uv, ua, speed)

        move.use_nitro = bool( world.tick > game.initial_freeze_duration_ticks and ua > 2 )
        move.brake = bool(ev < 0)
