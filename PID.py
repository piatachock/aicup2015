class PidRegulator :
    def __init__(self, k_p, k_ip, k_dp, t=1):
        self.e_1 = 0
        self.e_2 = 0
        self.u_1 = 0
        self.k_p = k_p
        self.k_di = k_p * k_ip * t
        self.k_dd = k_p * k_dp / t

    def control(self, e):
        u = self.u_1 + self.k_p * (e - self.e_1) + self.k_di * e + self.k_dd * (e - 2 * self.e_1 + self.e_2)
        self.u_1 = u
        self.e_2 = self.e_1
        self.e_1 = e
        return u