class Quadrant :
    size = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def middle(self):
        return self.at(0.5, 0.5)

    def at(self, xp, yp):
        size = Quadrant.size
        return (((self.x + xp) * size), ((self.y + yp) * size))

def get_quadrant(me):
    size = Quadrant.size
    return Quadrant(int(me.x / size), int(me.y / size))