from collections import defaultdict
from heapq import heappop, heappush
from Graph import Graph, Link, Vertex
from Quadrant import Quadrant
from math import *

class State :
    def __init__(self, a, v):
        self.a = a
        self.v = v

def dijkstra(vertexes, start, start_state, finish):
    def expand_path(path, v):
        if len(path) > 0 :
            expand_path(path[0], v)
            v.append(path[1])

    def get_cost(old_state, new_state):
        dv = new_state.v - old_state.v
        return abs(dv) if dv < 0 else 0

    def get_state(l, state):
        def angle(x, y):
            return atan(y/x) if x!=0 else asin(1)
        dx = float(l.vertex.quadrant.x - l.src.quadrant.x)
        dy = float(l.vertex.quadrant.y - l.src.quadrant.y)
        a = atan2(dy, dx) - state.a
        v = state.v * cos(2.0 * float(a))
        return State(a, v)

    marks = {}
    q = [(0, Link(start, 0, start), (), start_state)]
    while q:
        cost, link, path, state = heappop(q)
        v1 = link.vertex
        mark = marks.get(v1, None)
        if mark is None or mark > cost :
            marks[v1] = cost
            path = (path, link)

            if v1 == finish:
                retval = []
                expand_path(path, retval)
                return (cost, state, retval)

            for l in v1.links:
                new_state = get_state(l, state)
                total_cost = cost+l.weight+get_cost(state, new_state)
                mark = marks.get(l.vertex, None)
                if mark is None or mark > cost:
                    heappush(q, (total_cost, l, path, new_state))

    return float("inf")

if __name__ == "__main__":
    graph = Graph()
    a = graph.add_vertex("A", None, Quadrant(1,1))
    b = graph.add_vertex("B", None, Quadrant(1,2))
    c = graph.add_vertex("C", None, Quadrant(2,1))
    d = graph.add_vertex("D", None, Quadrant(2,2))
    e = graph.add_vertex("E", None, Quadrant(3,1))
    f = graph.add_vertex("F", None, Quadrant(3,2))
    g = graph.add_vertex("G", None, Quadrant(3,3))
    
    graph.link(a, b, 7)
    graph.link(a, d, 5)
    graph.link(b, c, 8)
    graph.link(b, d, 9)
    graph.link(b, e, 7)
    graph.link(c, e, 5)
    graph.link(d, e, 15)
    graph.link(d, f, 6)
    graph.link(e, f, 8)
    graph.link(e, g, 9)
    graph.link(f, g, 11)
    '''    
    edges = [
        ("A", "B", 7),
        ("A", "D", 5),
        ("B", "C", 8),
        ("B", "D", 9),
        ("B", "E", 7),
        ("C", "E", 5),
        ("D", "E", 15),
        ("D", "F", 6),
        ("E", "F", 8),
        ("E", "G", 9),
        ("F", "G", 11)
    ]'''

    print "=== Dijkstra ==="
    print graph.vertexes
    print "A -> E:"
    c, path = dijkstra(graph.vertexes, a, e)
    print c, [(l.weight, l.vertex.vid) for l in path]
    print "F -> G:"
    print dijkstra(graph.vertexes, f, g)